package com.example.dwork.csi.presenters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.example.dwork.csi.MainActivity;
import com.example.dwork.csi.models.NewsModel;
import com.example.dwork.csi.requests.NewsWebRequest;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class NewsPresenter {

    private MainActivity mainActivity;
    long requestDelayMillis = 3000;

    public NewsPresenter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void requestForNews(){

        final NewsModel newsModel = new NewsWebRequest(mainActivity).getNew();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainActivity.onGetNews(newsModel);
            }
        }, requestDelayMillis);
    }
}
