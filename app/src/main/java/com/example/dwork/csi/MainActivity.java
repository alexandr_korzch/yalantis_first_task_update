package com.example.dwork.csi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dwork.csi.models.NewsModel;
import com.example.dwork.csi.presenters.NewsPresenter;
import com.example.dwork.csi.util.OnClickListenerSetterFotTextViewsUtil;
import com.example.dwork.csi.adapters.NewsRecyclerAdapter;


/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class MainActivity extends AppCompatActivity {


    private ScrollView rootScrollView;

    private TextView mTitleTV;
    private TextView mStatusTV;
    private TextView mCreateHintTV;
    private TextView mCreateDateTV;
    private TextView mRegisterHintTV;
    private TextView mRegisterDateTV;
    private TextView mSolveHintTV;
    private TextView mSolveDateTV;
    private TextView mSolverHintTV;
    private TextView mSolverNameTV;
    private TextView mMainTextTV;

    private RecyclerView mRecyclerView;
    private NewsRecyclerAdapter mAdapter;

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getDispleyWidth();
        initRootElement();
        initActionBar();
        initTextViews();
        setOnClickListenerOnAllTextViews();
        initReciclerView();
        webRequest();
    }

    private void getDispleyWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        MyApplication.displayWidth = metrics.widthPixels;
    }

    private void initRootElement() {
        rootScrollView = (ScrollView)findViewById(R.id.root_sv);
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.newsTitle);
        }
    }

    private void initTextViews() {
        mTitleTV = (TextView) findViewById(R.id.mTitleTV);
        mStatusTV = (TextView) findViewById(R.id.mStatusTV);
        mCreateHintTV = (TextView) findViewById(R.id.mCreateHintTV);
        mCreateDateTV = (TextView) findViewById(R.id.mCreateDateTV);
        mRegisterHintTV = (TextView) findViewById(R.id.mRegisterHintTV);
        mRegisterDateTV = (TextView) findViewById(R.id.mRegisterDateTV);
        mSolveHintTV = (TextView) findViewById(R.id.mSolveHintTV);
        mSolveDateTV = (TextView) findViewById(R.id.mSolveDateTV);
        mSolverHintTV = (TextView) findViewById(R.id.mSolverHintTV);
        mSolverNameTV = (TextView) findViewById(R.id.mSolverNameTV);
        mMainTextTV = (TextView) findViewById(R.id.mMainTextTV);
    }

    private void initReciclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.pictures_rv);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new NewsRecyclerAdapter(getApplicationContext(), clickListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setOnClickListenerOnAllTextViews() {
        new OnClickListenerSetterFotTextViewsUtil(this).setOnClickListenerOnAllTextViews(clickListener);
    }

    private void webRequest() {
        new NewsPresenter(this).requestForNews();
        showProgressBar();
    }

    private void showProgressBar() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.load));
        mProgressDialog.setMessage(getString(R.string.please_wait));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void onGetNews(NewsModel newsModel) {
        showNews(newsModel);
    }

    public void showNews(NewsModel newsModel) {

        mProgressDialog.dismiss();
        rootScrollView.setVisibility(View.VISIBLE);

        mTitleTV.setText(newsModel.getTitleText());
        mStatusTV.setText(newsModel.getStatusText());
        mCreateHintTV.setText(newsModel.getCreateHint());
        mCreateDateTV.setText(newsModel.getCreateDate());
        mRegisterHintTV.setText(newsModel.getRegisterHint());
        mRegisterDateTV.setText(newsModel.getRegisterDate());
        mSolveHintTV.setText(newsModel.getSolveHint());
        mSolveDateTV.setText(newsModel.getSolveDate());
        mSolverHintTV.setText(newsModel.getSolverHint());
        mSolverNameTV.setText(newsModel.getSolverName());
        mMainTextTV.setText(newsModel.getMainText());

        mAdapter.setUrlList(newsModel.getUrlList());
        mAdapter.notifyDataSetChanged();
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String id_ = view.getResources().getResourceName(view.getId());
            String id_name = id_.substring(id_.lastIndexOf("/") + 1);
            Toast.makeText(MainActivity.this, id_name, Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.exitQuestion)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }
}



