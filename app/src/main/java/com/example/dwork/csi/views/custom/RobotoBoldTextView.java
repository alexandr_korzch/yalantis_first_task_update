package com.example.dwork.csi.views.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Alexandr on 26.03.2016.
 */
public class RobotoBoldTextView extends TextView{

    private String fontPath = "fonts/Roboto-Bold.ttf";

    public RobotoBoldTextView(Context context) {
        super(context);
        setFont(context);
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context){
        Typeface fontsStyle = Typeface.createFromAsset(context.getAssets(), fontPath);
        this.setTypeface(fontsStyle);
    }

}
