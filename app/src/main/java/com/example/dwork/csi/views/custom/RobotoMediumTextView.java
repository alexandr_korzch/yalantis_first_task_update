package com.example.dwork.csi.views.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Alexandr on 26.03.2016.
 */
public class RobotoMediumTextView extends TextView{

    private String fontPath = "fonts/Roboto-Medium.ttf";

    public RobotoMediumTextView(Context context) {
        super(context);
        setFont(context);
    }

    public RobotoMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public RobotoMediumTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context){
        Typeface fontsStyle = Typeface.createFromAsset(context.getAssets(), fontPath);
        this.setTypeface(fontsStyle);
    }
}
