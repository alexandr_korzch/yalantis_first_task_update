package com.example.dwork.csi.requests;

import android.content.Context;


import com.example.dwork.csi.R;
import com.example.dwork.csi.models.NewsModel;

import java.util.Arrays;

/**
 * Created by DWork on 3/13/2016.
 */
public class NewsWebRequest {

    private Context appContext;

    public NewsWebRequest(Context appContext) {
        this.appContext = appContext;
    }

    //retrofit
    public NewsModel getNew(){

        NewsModel newsModel = new NewsModel();

        newsModel.setTitleText(appContext.getResources().getString(R.string.titleText));
        newsModel.setStatusText(appContext.getResources().getString(R.string.statusText));
        newsModel.setCreateHint(appContext.getResources().getString(R.string.create));
        newsModel.setCreateDate(appContext.getResources().getString(R.string.createDate));
        newsModel.setRegisterHint(appContext.getResources().getString(R.string.registr));
        newsModel.setRegisterDate(appContext.getResources().getString(R.string.registrDate));
        newsModel.setSolveHint(appContext.getResources().getString(R.string.solvUntil));
        newsModel.setSolveDate(appContext.getResources().getString(R.string.solveDate));
        newsModel.setSolverHint(appContext.getResources().getString(R.string.solver));
        newsModel.setSolverName(appContext.getResources().getString(R.string.solverName));
        newsModel.setMainText(appContext.getResources().getString(R.string.news));

        newsModel.setUrlList(Arrays.asList(appContext.getResources().getStringArray(R.array.images)));

        return newsModel;

    }

}
