package com.example.dwork.csi.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.dwork.csi.MyApplication;
import com.example.dwork.csi.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.ViewHolder> {

    private List<String> urlList;
    private View.OnClickListener clickListener;
    private int imageWidth;

    public NewsRecyclerAdapter(Context context, View.OnClickListener clickListener) {
        this.clickListener = clickListener;
        imageWidth = getImageWidth(context);
    }

    private int getImageWidth(Context context) {
        float image_margin = context.getResources().getDimension(R.dimen.image_margin);
        float horizontal_margin = context.getResources().getDimension(R.dimen.fragment_horizontal_padding);
        int imageWidth = (int) ((MyApplication.displayWidth - image_margin - horizontal_margin) / 2);
        return imageWidth;
    }

    @Override
    public int getItemCount() {
        return urlList != null ? urlList.size() : 0;
    }

    @Override
    public NewsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item, parent, false);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        LinearLayout.LayoutParams params
                = new LinearLayout.LayoutParams(imageWidth, LinearLayout.LayoutParams.MATCH_PARENT);

        viewHolder.imageContainer.setLayoutParams(params);

        Uri uri = Uri.parse(urlList.get(position));
        viewHolder.imgViewIcon.setImageURI(uri);

        viewHolder.imgViewIcon.setOnClickListener(clickListener);
    }

    public void setUrlList(@NonNull List<String> urlList) {
        this.urlList = urlList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView imgViewIcon;
        private LinearLayout imageContainer;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imageContainer = (LinearLayout) itemLayoutView.findViewById(R.id.imageContainer);
            imgViewIcon = (SimpleDraweeView) itemLayoutView.findViewById(R.id.imgViewIcon);
        }
    }
}