package com.example.dwork.csi.models;

import java.util.List;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class NewsModel {


    private String titleText;
    private String statusText;
    private String createHint;
    private String createDate;
    private String registerHint;
    private String registerDate;
    private String solveHint;
    private String solveDate;
    private String solverHint;
    private String solverName;
    private String mainText;

    private List<String> urlList;

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getCreateHint() {
        return createHint;
    }

    public void setCreateHint(String createHint) {
        this.createHint = createHint;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getRegisterHint() {
        return registerHint;
    }

    public void setRegisterHint(String registerHint) {
        this.registerHint = registerHint;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getSolveHint() {
        return solveHint;
    }

    public void setSolveHint(String solveHint) {
        this.solveHint = solveHint;
    }

    public String getSolveDate() {
        return solveDate;
    }

    public void setSolveDate(String solveDate) {
        this.solveDate = solveDate;
    }

    public String getSolverHint() {
        return solverHint;
    }

    public void setSolverHint(String solverHint) {
        this.solverHint = solverHint;
    }

    public String getSolverName() {
        return solverName;
    }

    public void setSolverName(String solverName) {
        this.solverName = solverName;
    }

    public String getMainText() {
        return mainText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<String> urlList) {
        this.urlList = urlList;
    }
}
