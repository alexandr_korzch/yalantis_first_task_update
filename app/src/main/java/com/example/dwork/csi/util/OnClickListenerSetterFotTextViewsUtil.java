package com.example.dwork.csi.util;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandr on 26.03.2016.
 */
public class OnClickListenerSetterFotTextViewsUtil {

    private Activity mActivity;

    public OnClickListenerSetterFotTextViewsUtil(Activity mActivity) {
        this.mActivity = mActivity;
    }

    public void setOnClickListenerOnAllTextViews(View.OnClickListener clickListener) {
        List<TextView> textViews = new ArrayList();
        ViewGroup viewGroup = (ViewGroup) mActivity.getWindow().getDecorView();
        findTextViews(viewGroup, textViews);

        for(TextView textView: textViews){
            textView.setOnClickListener(clickListener);
        }
    }

    private void findTextViews(ViewGroup viewGroup, List<TextView> textViews) {
        for (int i = 0, N = viewGroup.getChildCount(); i < N; i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof ViewGroup) {
                findTextViews((ViewGroup) child, textViews);
            } else if (child instanceof TextView) {
                textViews.add((TextView) child);
            }
        }
    }
}
